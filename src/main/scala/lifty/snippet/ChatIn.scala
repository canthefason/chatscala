import net.liftweb.http.js.JsCmds.SetValById
import net.liftweb.http.SHtml

object ChatIn {
  def render = SHtml.onSubmit( s => {
    ChatServer ! s
    SetValById("chat_in", "")
  })
}
