import java.util
import java.util._
import net.liftweb.http.{CometListener, CometActor}

class Chat extends CometActor with CometListener {
  private var msgs: Vector[String] = util.Vector()

  def registerWith = ChatServer

  override def lowPriority = {
    case v: Vector[String] => msgs = v; reRender()
  }

  def render = "li *" #> msgs & ClearClearable
}